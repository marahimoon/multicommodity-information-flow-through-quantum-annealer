# Multicommodity Information Flow Through Quantum Annealer



****Name of Author**** Munawar Ali

**Brief Description:** The research project titled "Multicommodity Information Flow Through Quantum Annealer" was conducted during my Master's program at the National University of Sciences and Technology (NUST), Islamabad and has been documented in detail in the corresponding article.


The **[Multicommodity Information Flow Through Quantum Annealer](https://gitlab.com/marahimoon/multicommodity-information-flow-through-quantum-annealer/-/tree/main/Multicommodity%20Information%20Flow%20Through%20Quantum%20Annealer?ref_type=heads)** directory comprises the following sub-directories and files.

**[Instances folder](https://gitlab.com/marahimoon/multicommodity-information-flow-through-quantum-annealer/-/tree/main/Multicommodity%20Information%20Flow%20Through%20Quantum%20Annealer/Instances?ref_type=heads)** The folder contains the following scripts: [instances_generator.py](https://gitlab.com/marahimoon/multicommodity-information-flow-through-quantum-annealer/-/blob/main/Multicommodity%20Information%20Flow%20Through%20Quantum%20Annealer/Instances/inctances_generator.py?ref_type=heads), [instances.py](https://gitlab.com/marahimoon/multicommodity-information-flow-through-quantum-annealer/-/blob/main/Multicommodity%20Information%20Flow%20Through%20Quantum%20Annealer/Instances/instances.py?ref_type=heads), and [supply_demand.py](https://gitlab.com/marahimoon/multicommodity-information-flow-through-quantum-annealer/-/blob/main/Multicommodity%20Information%20Flow%20Through%20Quantum%20Annealer/Instances/supply_demand_nodes.py?ref_type=heads). These scripts are designed to generate random ODIMCF instances and save the data to text files. 

**[ODIMCF.py](https://gitlab.com/marahimoon/multicommodity-information-flow-through-quantum-annealer/-/blob/main/Multicommodity%20Information%20Flow%20Through%20Quantum%20Annealer/ODIMCF.py?ref_type=heads)** This encapsulated our primary class functionalities. It is specifically designed to generate the QUBO representation for the ODIMCF problem and possesses the capability to solve it using various solver such as Simulated annealing, Quantum annealing and Hybrid solvers.

**[gurobi.py](https://gitlab.com/marahimoon/multicommodity-information-flow-through-quantum-annealer/-/blob/main/Multicommodity%20Information%20Flow%20Through%20Quantum%20Annealer/gurobi.py?ref_type=heads)** This file have implemented an algorithm to address addresses both the standard form of the ODIMCF problem and its formulation as a QUBO problem using Gurobi solver. The solution obtained using the Gurobi solver for the standard form of ODIMCF is considered the exact solution. 

**[solvers.py](https://gitlab.com/marahimoon/multicommodity-information-flow-through-quantum-annealer/-/blob/main/Multicommodity%20Information%20Flow%20Through%20Quantum%20Annealer/solvers.py?ref_type=heads)** This file serves as an interface, invoking various samplers. The primary functionalities and logic for these samplers are encapsulated within the the ODIMCF.py class file, which addresses the ODIMCF problem instances.




**To utilize the Gurobi sampler invoke the following command within the [solvers.py](https://gitlab.com/marahimoon/multicommodity-information-flow-through-quantum-annealer/-/blob/main/Multicommodity%20Information%20Flow%20Through%20Quantum%20Annealer/solvers.py?ref_type=heads) module:**

sampler = sampler = 'GUROBI'

or

sampler = sampler = 'GUROBI_QUBO'


fqs = GUROBI(T_commodities, source_nodes, dest_nodes, sampler)




**To utilize the Quantum Annealer, Simulated Annealer, and Hybrid Solvers invoke the following command within the [solvers.py](https://gitlab.com/marahimoon/multicommodity-information-flow-through-quantum-annealer/-/blob/main/Multicommodity%20Information%20Flow%20Through%20Quantum%20Annealer/solvers.py?ref_type=heads) module:**

sampler = sampler = 'BQM'

sampler = sampler = 'SA'

sampler = sampler = 'Hybrid'


fqs = FQS(T_commodities, source_nodes, dest_nodes, sampler)





**The [Results](https://gitlab.com/marahimoon/multicommodity-information-flow-through-quantum-annealer/-/tree/main/Results?ref_type=heads) directory comprises the [experiment results.ods](https://gitlab.com/marahimoon/multicommodity-information-flow-through-quantum-annealer/-/blob/main/Results/experiment%20results.ods?ref_type=heads) and [results.odt](https://gitlab.com/marahimoon/multicommodity-information-flow-through-quantum-annealer/-/blob/main/Results/results.odt?ref_type=heads) files and [images](https://gitlab.com/marahimoon/multicommodity-information-flow-through-quantum-annealer/-/tree/main/Results/images?ref_type=heads) sub-directory which contains our experiment results of ODIMCF problem instances.** 
