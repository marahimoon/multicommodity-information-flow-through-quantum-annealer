from pprint import pprint
import matplotlib.pyplot as plt
import numpy as np
from pyqubo import *
import math
import random
import networkx as nx
import time




class FQS():

	def __init__(self, T_commodities, source_nodes, dest_nodes, sampler):
		
		self.T_commodities = T_commodities
		self.source_nodes = source_nodes
		self.dest_nodes = dest_nodes
		self.sampler = sampler

		self.sampleset = None
		self.best_sample = None
		self.sol = None
		self.activeKeys = None
		self.broken_constraints = None
		self.time = None

		#self.model = ConstrainedQuadraticModel()
		self.timing = {}

		self.clock = time.time() # Begin stopwatch
		self.formulate() # formulate problem
		self.timing['formulation_time'] = (time.time() - self.clock)
		


	
	def formulate(self):
		G = nx.read_edgelist('Instances/graph.txt', create_using = nx.DiGraph())
		#nx.draw(G)
		'''
			Define the binary variables X_i_j_k 
		                       
									'''

		x = [{(i, j): [Binary('x_{0}_{1}_{2}'.format(i, j, k)) ] for (i, j) in G.edges} for k in range(self.T_commodities)]



		'''  
		         Assign the demands to each commodity  
		                                                 
		                                                    '''
		comm_dem = np.loadtxt('Instances/comm_dem.txt')



		'''  
		         Define the the cost the cost matrix C for each arc link  
		                                                                    
		                                                                    '''

		cost = np.loadtxt('Instances/cost.txt')
		if cost.shape == (1,G.number_of_edges()):
		   cost = np.array([np.loadtxt('Instances/cost.txt')])



		'''  
		         Assign the cost to each variable link with commodity demands  
		                                                                       
		                                                                         '''

		sum_cost_x = sum([(np.matmul(list(sum(x[k][arc]) for arc in G.edges), cost[k]) * comm_dem[k]) 
for k in range(self.T_commodities)])


		'''  
		         Define the variables associate with edges which out from the 
		         nodes i and the edges which into the nodes i.  
                                                                    
		                                                                        '''

		inn = [[[sum(x[k][(i, j)]) for (i, j) in G.in_edges(str(nodes))] for nodes in range(G.number_of_nodes())] for k in range(self.T_commodities)]

		out = [[[sum(x[k][(i, j)]) for (i, j) in G.out_edges(str(nodes))] for nodes in range(G.number_of_nodes())] for k in range(self.T_commodities)]




#---------------------------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------

		'''
                             
		                               The constraints of the problem
		                    
		                                                                          '''
                                                                            

		# sum of out nodes - sum of in nodes = b_k...  balanced constraint

		def sum_out_in(self):
			
			'''

		         	 sum of supply and demand nodes. or nodes with source or destination points... 
		         	 sum((sum_out - sum_in - d) ** 2) or sum((sum_out - sum_in + d) ** 2)
                                		                     
				
												 '''
			diff_k_out_in_s, diff_k_out_in_d, diff_k_out_in = [] , [], []
			for k in range(self.T_commodities):

        			diff_out_in_s, diff_out_in_d, diff_out_in = [] , [], []

        			for nodes in range(G.number_of_nodes()):
	
            				sum_in, sum_out = 0, 0
            				sum_in = sum(inn[k][nodes])
            				sum_out = sum(out[k][nodes])
	
            				if nodes in self.source_nodes[k]:
	
               					diff_out_in_s.append((sum_out - sum_in - 1) ** 2)

            				if nodes in self.dest_nodes[k]:

               					diff_out_in_d.append((sum_out - sum_in + 1) ** 2)
             
            				if nodes not in list(set(self.dest_nodes[k])) and nodes not in list(set(self.source_nodes[k])):

               					diff_out_in.append((sum_out - sum_in) ** 2)
             
        			diff_k_out_in_s.append(sum(diff_out_in_s)), diff_k_out_in_d.append(sum(diff_out_in_d)), diff_k_out_in.append(sum(diff_out_in))
			
			sum_diff_k_s = sum(diff_k_out_in_s)
			sum_diff_k_d = sum(diff_k_out_in_d)
			sum_diff_k = sum(diff_k_out_in)

			return sum_diff_k, sum_diff_k_s, sum_diff_k_d


		sum_diff_k, sum_diff_k_s, sum_diff_k_d = sum_out_in(self)


		'''  

			Capacity constraint with slack variables
          
                        	      
                        		                                 '''

		def cap_constraint(arcs, self, comm_dem, x):

    			#capacity = {(i, j): math.ceil(10 * np.random.random()) for (i, j) in arcs}
    			#capacity = {(i, j): math.ceil(5) for (i, j) in arcs}
    			capcit = np.loadtxt('Instances/capacity.txt')
    			capacity = {(i, j): int(capcit[edges]) for (i, j),edges in zip(G.edges, range(G.number_of_edges()))}
    			arc_variable = {}  # arcs variables for different commodities
    			[[arc_variable.setdefault((i, j), []).append(sum(x[k][(i, j)])*comm_dem[k]) for k in range(self.T_commodities)] for (i, j) in arcs]
    			slack_variables = {arc: UnaryEncInteger("s_{0}".format(arc), (0, capacity[arc])) for arc in arcs}
   			 #cap = [(sum(arc_variable[arc]) + slack_variables[arc] - capacity[arc]) ** 2 for arc in arcs]
    			cap = [(-sum(arc_variable[arc]) + slack_variables[arc]) ** 2 for arc in arcs]
    			
    			return sum(cap), capacity, arc_variable, slack_variables


		cap_constraint, capacity, arc_variable, slack_variables = cap_constraint(G.edges, self, comm_dem, x)




		


#------------------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------------------
		'''                           
                                         	Qubo formation
		
	                                                                      '''
		
		'''  
          		Defines the penalty constant M and M1
                              
                        	                              '''                                                   

		M =  math.floor(sum(comm_dem[k] for k in range(self.T_commodities))/self.T_commodities * sum(sum(cost[k])/G.number_of_edges() for k in range(self.T_commodities))/self.T_commodities)
		M1 = math.ceil(sum(max(cost[k]) for k in range(self.T_commodities))/self.T_commodities/3)  

		M =  math.ceil(sum(comm_dem[k] for k in range(self.T_commodities))/self.T_commodities) * math.ceil(sum(sum(cost[k])/G.number_of_edges() for k in range(self.T_commodities))/self.T_commodities ) +2
		M1 = math.floor(sum(sum(cost[k])/G.number_of_edges() for k in range(self.T_commodities))/self.T_commodities)

		M =  2*10*1     #commodity*10*k
		M1 = 2          # comdodity #k
		# -------- H is an unconstraint objective function ---------- 
		''' 
		           sum_cost_x --->> objective function
	           sum_diff_k, sum_diff_k_s, sum_diff_k_d, cap_constraint ---->> constraints formulated as penalties
        	   M and M1 are penalty constants 
        		                                  ''' 

		H_A = sum_cost_x
		H_B = Constraint(M * sum_diff_k + M * sum_diff_k_s + M * sum_diff_k_d , label='H_B')
		#H_C = Constraint(M1*cap_constraint , label='H_C')


		H = H_A + H_B #+ H_C
		#H = sum_cost_x + (M * sum_diff_k + M * sum_diff_k_s + M * sum_diff_k_d) +  (M1*cap_constraint) 



		H_A = Constraint(sum_diff_k_s, label='H_A')
		H_B = Constraint(sum_diff_k_d, label='H_B')
		H_C = Constraint(sum_diff_k, label='H_C')
		H_D = Constraint(cap_constraint , label='H_D')
		H_E = sum_cost_x
		#H = H_A + M * H_B + M * H_C + M1 * H_D +  H_E
		H =  M * H_A + M * H_B + 1*M * H_C + H_E + M1 * H_D
		# -------- creating QUBO/BQM with PyQubo Python library --------

		model = H.compile()
		qubo, offset = model.to_qubo()
		bqm = model.to_bqm()


# ----------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------
                     
		# -------- solving bqm with classical solver simulatted annealing ----------

		print('start')

		def solve(self, bqm, numruns=100):
			if self.sampler == 'SA':		
				sampler = neal.SimulatedAnnealingSampler()
				t_i = time.perf_counter()
				self.sampleset = sampler.sample(bqm, num_reads=numruns)#, num_sweeps=1000)#, beta_range=[0.99/1, 4.99/1])
				t_f = time.perf_counter()
				self.time = 'SA runtime', t_f - t_i

			if self.sampler == 'BQM':
				from dwave.system.samplers import DWaveSampler
				from dwave.system.composites import EmbeddingComposite
				# Set up QPU parameters
				chainstrength = M1*5
				sampler = EmbeddingComposite(DWaveSampler(solver={'topology__type': 'chimera'}, profile="as"))
				self.sampleset = sampler.sample(bqm,
				                               chain_strength=chainstrength,
				                               num_reads=numruns,
				                               label='Example - commodity')
				
				#self.time = 'qpu access time', [sampleset.info['timing']['qpu_access_time']]

			if self.sampler == 'Hybrid':
				from dwave.system import LeapHybridSampler
				solver = LeapHybridSampler.default_solver
				solver.update(name__regex=".*(?<!bulk)$")
				sampler = LeapHybridSampler(solver=solver, profile="as")
				self.sampleset = sampler.sample(bqm)
				#self.time = 'Hybrid solver runtime', sampleset.info['run_time']

			decoded_samples = model.decode_sampleset(self.sampleset)
			self.best_sample = min(decoded_samples, key=lambda x: x.energy)
			#pprint(best_sample.sample)
			#print(sampleset)
			#print(best_sample.energy)
			#return(self.sampleset)
		solve = solve(self, bqm)





		x_i_j_k = ['x_{0}_{1}_{2}'.format(i, j, k) for (i, j) in G.edges() for k in range(self.T_commodities)]
		def get_key(self, val):
    			keys = []
    			for key, value in self.best_sample.sample.items():
         			if val == value and key in x_i_j_k:
             				keys.append(key)
    			self.activeKeys = keys
		get_key = get_key(self, 1)
		#pprint(get_key(1))



		decoded_sample = model.decode_sample(self.sampleset.first.sample, vartype="BINARY")
		self.broken_constraints = decoded_sample.constraints(only_broken=True)



	
		print()


		
		
