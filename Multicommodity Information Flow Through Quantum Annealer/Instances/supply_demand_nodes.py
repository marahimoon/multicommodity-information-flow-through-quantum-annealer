import networkx as nx
from random import random, choice




T_commodities = 3  # Number of different types of commodities



def Source_dest_nodes(T_commodities):

	G = nx.read_edgelist('graph.txt', nodetype=int, create_using = nx.DiGraph())
	n = G.nodes
	thresnet = 0.75  # density of network
	thresdem = 0.8  # density of demand mesh

# This function finds all possible paths from source to destination

	def find_paths(G, n):

    		def find_path(G, start, end, path=[]):  # find all paths from source to destination

    		    path = path + [start]
    		    if start == end:
    		        return path
    		    if start not in G:
        	   	 return None
    		    for node in G[start]:
    		        if node not in path:
    		            newpath = find_path(G, node, end, path)
    		            if newpath: return newpath

    		    return None


    		all_paths = []

    		for start in range(n):
        		for end in range(n):
        		    if end != start:
        		        all_paths.append(find_path(G, start, end))

    		return list(filter(None, all_paths))

	G1 = {n: list(G.adj[n]) for n in G.nodes}
	all_paths = find_paths(G1, max(G.nodes) + 1)  # call function for all paths and all possible paths.


	# Create random demand between node pairs

	def supply_demand(n, T_commodities, all_paths, thresdem):

    		source_nodes1, dest_nodes1 = [], []

    		for k in range(T_commodities):
        		source_nodes, dest_nodes, c = [], [], []
        		#for i in range(n):
            			#if np.random.random() < thresdem:
        		a = choice(all_paths)
        		d = (a[0], a[-1])
        		e = (a[-1], a[0])
        		if d not in c:
           			if e not in c:
              				if a[-1] not in source_nodes and a[0] not in dest_nodes:
                 				c.append(d), source_nodes.append(a[0]), dest_nodes.append(a[-1])
        		source_nodes1.append(source_nodes), dest_nodes1.append(dest_nodes)

    		return source_nodes1, dest_nodes1


	source_nodes, dest_nodes = supply_demand(n, T_commodities, all_paths, thresdem)

	return source_nodes, dest_nodes

#source_nodes, dest_nodes = Source_dest_nodes(T_commodities)

#print(source_nodes)
#print(dest_nodes)

#print()

#avg_degree = sum([v for k, v in G.degree()])/G.number_of_nodes()
#print('avg_degree = ', avg_degree)

#p = 2 * G.number_of_edges() / ( G.number_of_nodes()*(G.number_of_nodes() - 1) ) * 100

#print('edge percentage = ', p,'%')







